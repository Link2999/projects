﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

	public GameObject[] hazards;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText gameOverText;
	public GUIText pauseText;
	public GUIText livesText;
	public GUIText restartText;
	public GUIText menuText;
	public GUIText highscoreText;
	public GUIText newhsText;

	public static bool gameOver;
	private bool restart;
	private bool paused;
	private bool pausable;
	private int score;
	private int lives;
	public static bool greenPower = false;

	private AudioSource source;
	public AudioClip gameLose;
	public AudioClip lifeLost;
	public AudioClip explosionSound;
	public AudioClip laserSound;
	public float fxScale;

	void Start () {
		source = GetComponent<AudioSource> ();
		gameOver = false;
		restart = false;
		paused = false;
		pausable = true;
		gameOverText.text = "";
		pauseText.text = "";
		restartText.text = "";
		menuText.text = "";
		highscoreText.text = "";
		newhsText.text = "";
		score = 0;
		lives = 4;
		UpdateScore ();
		UpdateLives ();
		StartCoroutine (SpawnWaves ());
	}
	
	void Update ()
	{
		if (restart || paused) {
			restartText.text = "Press 'R' for Restart";
			menuText.text = "Press 'M' for Menu";
			if (Input.GetKeyDown (KeyCode.R)) {
				SceneManager.LoadScene (1);
				pauseGame (false);
			}
			if (Input.GetKeyDown (KeyCode.M)) {
				SceneManager.LoadScene ("Start_Screen");
				pauseGame (false);
			}
		}
		if (pausable && Input.GetKeyDown (KeyCode.P)) {
			pauseGame (!paused);
		}
	}

	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds (startWait);
		while(true)
		{
			for(int i = 0; i < hazardCount; i++)
			{
				GameObject hazard = hazards [Random.Range (0, hazards.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Debug.Log (spawnPosition.ToString());
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver) {
				break;
			}
		}
	}

	public void AddScore(int newScoreValue)
	{
		if (greenPower == false)
		{
			score += newScoreValue;
			UpdateScore();
		}
		if (greenPower == true)
		{
			score += 2 * newScoreValue; // multiply score by 2
			UpdateScore();
		}
	}

	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
		highscoreText.text = "High Score: " + PlayerPrefs.GetInt ("highscore", 0);
	}

	public void LoseLife()
	{
		Debug.Log ("Lost a life!");
		if (lives > 0) {
			lives--;
			source.PlayOneShot (lifeLost, fxScale);
		}
		UpdateLives ();
		if (lives == 0 && !gameOver)
			this.GameOver ();
	}

	void UpdateLives()
	{
		livesText.text = "Lives: " + lives;
	}

	public void playExplosionSound()
	{
		source.PlayOneShot (explosionSound, fxScale);
	}

	public void playLaserSound()
	{
		source.PlayOneShot (laserSound, fxScale);
	}

	public void GameOver()
	{
		source.PlayOneShot (gameLose, fxScale);
		lives = 0;
		UpdateLives ();
		StoreHighScore (score);
		Destroy (GameObject.FindWithTag ("Player"));
		Destroy (GameObject.FindWithTag ("Bolt"));
		Destroy (GameObject.FindWithTag ("Base"));
		Destroy (GameObject.FindWithTag ("Timer"));
		gameOverText.text = "Game Over!";
		pausable = false;
		gameOver = true;
		restart = true;
	}

	void StoreHighScore(int newhs)
	{
		int oldhs = PlayerPrefs.GetInt ("highscore", 0);
		if (newhs > oldhs) {
			PlayerPrefs.SetInt ("highscore", newhs);
			newhsText.text = "New High Score!";
		}
	}

	public void pauseGame(bool pauseState)
	{
		paused = pauseState;
		if (paused) {
			restartText.text = "Press 'R' for Restart";
			menuText.text = "Press 'M' for Menu";
			Time.timeScale = 0;
			pauseText.text = "Paused";
			source.Pause ();
		} else {
			restartText.text = "";
			menuText.text = "";
			Time.timeScale = 1;
			pauseText.text = "";
			source.UnPause ();
		}
	}

	public bool isPaused()
	{
		return paused;
	}

	public bool isGameOver()
	{
		return gameOver;
	}

	public int getLives()
	{
		return lives;
	}

	public static void GreenPowerUpOn()
	{
		greenPower = true;

	}
	public static void GreenPowerUpOff()
	{
		greenPower = false;

	}
}
