﻿using UnityEngine;
using System.Collections;

public class PowerUpActivate : MonoBehaviour {

	public GameObject explosion;
    public GameObject timer;
	public int scoreValue;
	private GameController gameController;
    public GameObject shield;


    void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
			gameController = gameControllerObject.GetComponent<GameController> ();
		if (gameController == null)
			Debug.Log ("Cannot find 'GameController' script");
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Bolt") {
			switch (this.tag) {
			case "RedPowerUp": // firerate power up
				PlayerController.incFireRate ();
				gameController.AddScore (scoreValue);
				if(explosion != null)
					Instantiate (explosion, transform.position, transform.rotation);
                    Instantiate(timer, transform.position, transform.rotation);
                    gameController.playExplosionSound ();
				Destroy (other.gameObject);
				Destroy (gameObject);
				break; 
            case "BluePowerUp": // shield power up
                    gameController.AddScore(scoreValue);
                    Instantiate(shield);
                    Instantiate(explosion, transform.position, transform.rotation);
                    Instantiate(timer, transform.position, transform.rotation);
                    gameController.playExplosionSound();
                    Destroy(other.gameObject);
                    Destroy(gameObject);
                    break;
			case "GreenPowerUp": // 2x multiplier power up
				gameController.AddScore(scoreValue);
				GameController.GreenPowerUpOn();
				PlayerController.greenPowerOn();
				Instantiate(explosion, transform.position, transform.rotation);
				Instantiate(timer, transform.position, transform.rotation);
				gameController.playExplosionSound();
				Destroy(other.gameObject);
				Destroy(gameObject);
				break; 


                default:
				break;
			}
			return;
		}
		else if(other.tag == "Hazard" || other.tag == "Boundary")
			return;
		else
		{
			if(explosion != null)
				Instantiate (explosion, transform.position, transform.rotation);
			gameController.playExplosionSound ();
			Destroy (gameObject);
		}
	}
}
