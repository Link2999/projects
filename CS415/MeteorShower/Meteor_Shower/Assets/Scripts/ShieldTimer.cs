﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShieldTimer : MonoBehaviour
{

    Image timerBar;
    float time; // timer for powerUp
    float timeAmt = 10;

    void Start()
    {
        timerBar = this.GetComponent<Image>();
        time = timeAmt;
    }


    // Update is called once per frame
    void Update()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
            timerBar.fillAmount = time / timeAmt;
        }
    }


}


