﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

	public GameObject explosion;
	public int scoreValue;
	private GameController gameController;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
			gameController = gameControllerObject.GetComponent<GameController> ();
		if (gameController == null)
			Debug.Log ("Cannot find 'GameController' script");
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		switch (other.tag) {
		case "Boundary":
			break;
		case "Hazard":
			if (explosion != null)
				Instantiate (explosion, transform.position, transform.rotation);
			gameController.playExplosionSound ();
			Destroy (gameObject);
			break;
		case "Player":
		case "Ground":
			gameController.LoseLife ();
			if(explosion != null)
				Instantiate (explosion, transform.position, transform.rotation);
			gameController.playExplosionSound ();
			Destroy (gameObject);
			break;
            case "shield":
                if (explosion != null)
                    Instantiate(explosion, transform.position, transform.rotation);
                gameController.playExplosionSound();
                Destroy(gameObject);
                break;
		default:
			gameController.AddScore (scoreValue);
			if(explosion != null)
				Instantiate (explosion, transform.position, transform.rotation);
			gameController.playExplosionSound ();
			Destroy (other.gameObject);
			Destroy (gameObject);
			break;
		}
	}
}