﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private GameController gameController;
	public GameObject shot;
	public GameObject redShot;
	public GameObject greenShot;
	public Transform shotSpawn;
	private Vector3 rot;

	private float nextFire;
	public float speed;
	private static bool redPower;
	private static bool greenPower;
	private static float fireRate;

	void Start()
	{
		redPower = false;
		greenPower = false;
		fireRate = 0.45f;

		rot = new Vector3 (0.0f, 0.0f, 0.0f);
		transform.rotation = Quaternion.Euler (rot);
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
			gameController = gameControllerObject.GetComponent<GameController> ();
		if (gameController == null)
			Debug.Log ("Cannot find 'GameController' script");
	}

	void Update()
	{
		if ((Input.GetKey (KeyCode.Space) || Input.GetKey(KeyCode.LeftShift)) && Time.time > nextFire && !gameController.isPaused() && redPower == false && greenPower == false) {
			nextFire = Time.time + fireRate;
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
			gameController.playLaserSound ();
		}

		if ((Input.GetKey (KeyCode.Space) || Input.GetKey(KeyCode.LeftShift)) && Time.time > nextFire && !gameController.isPaused() && redPower == true && greenPower == false) {
			nextFire = Time.time + fireRate;
			Instantiate (redShot, shotSpawn.position, shotSpawn.rotation);
			gameController.playLaserSound ();
		}

		if ((Input.GetKey (KeyCode.Space) || Input.GetKey(KeyCode.LeftShift)) && Time.time > nextFire && !gameController.isPaused() && greenPower == true && redPower == false) {
			nextFire = Time.time + fireRate;
			Instantiate (greenShot, shotSpawn.position, shotSpawn.rotation);
			gameController.playLaserSound ();
		}
	}

	void FixedUpdate()
	{
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			rot = new Vector3 (0, 0, Mathf.Clamp (rot.z + (0.5f * speed), -70.0f, 70.0f));
			transform.rotation = Quaternion.Euler (rot);
		}

		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
			rot = new Vector3 (0, 0, Mathf.Clamp (rot.z - (0.5f * speed), -70.0f, 70.0f));
			transform.rotation = Quaternion.Euler (rot);
		}
	}


	public static void incFireRate()
	{
		redPower = true;
		fireRate = 0.225f;
	}

	public static void decFireRate()
	{
		redPower = false;
		fireRate = 0.45f;
	}

	public static void greenPowerOn()
	{
		greenPower = true;
	}

	public static void greenPowerOff()
	{
		greenPower = false;
	}
}
