﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PowerUpController : MonoBehaviour
{

	public GameObject[] powerups;
	public Vector3 spawnValues;
	public int powerupCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	void Start () {
		StartCoroutine (SpawnWaves ());
	}
		
	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds (startWait);
		while(true)
		{
			for(int i = 0; i < powerupCount; i++)
			{
				GameObject powerup = powerups [Random.Range (0, powerups.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Debug.Log (spawnPosition.ToString());
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (powerup, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (GameController.gameOver) {
				break;
			}

		}
	}
		
}