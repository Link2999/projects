﻿using UnityEngine;
using System.Collections;

public class FlagController : MonoBehaviour {

	public GameObject flag;
	public Vector3 spawnValues;
//	public GameObject[] flags;
	private GameController gameController;

	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
			gameController = gameControllerObject.GetComponent<GameController> ();
		if (gameController == null)
			Debug.Log ("Cannot find 'GameController' script");
	}

	void Update () {
		GameObject[] flags = GameObject.FindGameObjectsWithTag ("Flag");
		for (int i = 0; i < flags.Length; i++)
			Destroy (flags [i]);
		Vector3 spawnPosition;
		for (int i = 0; i < gameController.getLives (); i++) {
			if(i > 1)
				spawnPosition = new Vector3(spawnValues.x - (i * 1.0f) - 2, spawnValues.y, spawnValues.z);
			else
				spawnPosition = new Vector3 (spawnValues.x - (i * 1.0f), spawnValues.y, spawnValues.z);
			Quaternion spawnRotation = Quaternion.identity;
			Instantiate (flag, spawnPosition, spawnRotation);
		}
	}
}
