﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour
{
	public float timer = 2.8f;

	void Update ()
	{
		timer -= Time.deltaTime;
		if (timer <= 0) {
			Debug.Log ("Boom!");
			Destroy (gameObject);
		}
	
	}
}
