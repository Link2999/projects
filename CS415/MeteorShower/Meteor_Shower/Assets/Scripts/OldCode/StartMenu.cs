﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{

    public Canvas quitMenu;
    public Button startText;
    public Button exitText;

	private AudioSource source;
	public AudioClip startSound;
	public AudioClip endSound;

    // Use this for initialization
    void Start()
    {
		source = GetComponent<AudioSource> ();
        quitMenu = quitMenu.GetComponent<Canvas>();
        startText = startText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        quitMenu.enabled = false;
    }

    public void ExitPress()
    {
        quitMenu.enabled = true;
        startText.enabled = false;
        exitText.enabled = false;
    }

    public void NoPress()
    {
        quitMenu.enabled = false;
        startText.enabled = true;
        exitText.enabled = true;
    }

	public void StartLevel()
    {
		StartCoroutine (playSoundThenBegin ());
    }
    public void ExitGame()
	{
		StartCoroutine (playSoundThenQuit ());
	}

	IEnumerator playSoundThenBegin()
	{
		source.PlayOneShot (startSound, 1);
		yield return new WaitForSeconds (startSound.length);
		SceneManager.LoadScene (1);
	}

	IEnumerator playSoundThenQuit()
	{
		source.PlayOneShot (endSound, 1);
		yield return new WaitForSeconds (endSound.length);
		Application.Quit ();
	}
}
