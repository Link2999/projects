﻿using UnityEngine;
using System.Collections;

public class ProjectileDmgHandler : MonoBehaviour
{
	public int health = 1;

	void OnCollisionEnter2D ()
	{
		Debug.Log ("Projectile Collision!");
		health--;
	}

	void Update ()
	{
		if (health <= 0) {
			Destroy (gameObject);
		}
	}
		
}