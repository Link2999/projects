﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
	private Vector3 rot;
	public float speed;

	void Start ()
	{
		rot = gameObject.transform.rotation.eulerAngles;
	}

	void Update ()
	{
		if (Time.timeScale != 0) {

			if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
				rot.z = Mathf.Clamp (rot.z + 1.4f, -60f, 60f);
				transform.rotation = Quaternion.Euler (rot);
			}

			if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
				rot.z = Mathf.Clamp (rot.z - 1.4f, -60f, 60f);
				transform.rotation = Quaternion.Euler (rot);
			}
		}

	}
}