﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour
{

    public bool Pause;
    public Canvas pausemenu;

    void Start()
    {
        pausemenu = pausemenu.GetComponent<Canvas>();
        Pause = false;
        pausemenu.enabled = false;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Pause = !Pause;
        }
        if(Pause == true)
        {
            Time.timeScale = 0;
            pausemenu.enabled = true;
        }
        else if (!Pause)
        {
            Time.timeScale = 1;
            pausemenu.enabled = false;

        }
    }
}