﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{
	public GameObject bulletPrefab;
	public AudioSource audioSource;
	public AudioClip pewSound;
	public float fireDelay = 0.5f;
	public float cdTimer = 0;


	void Update ()
	{
		cdTimer -= Time.deltaTime;

		if (Input.GetButton ("Fire1") && cdTimer <= 0 && Time.timeScale != 0) {
			Debug.Log ("Pew!");
			cdTimer = fireDelay;

			audioSource = GetComponent<AudioSource> ();
			audioSource.clip = pewSound;
			audioSource.Play ();

			Vector3 offset = transform.rotation * new Vector3 (0, 0.5f, 1);
			Instantiate (bulletPrefab, transform.position + offset, transform.rotation);
		}
	}
}
