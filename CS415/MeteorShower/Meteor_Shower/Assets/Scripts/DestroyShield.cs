﻿using UnityEngine;
using System.Collections;

public class DestroyShield : MonoBehaviour
{


    float lifetime = 0;
    float time; 
    float timeAmt = 10;

    void Start()
    {
        time = timeAmt;
    }


    // Update is called once per frame
    void Update()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
        }
        if (time < 0)
            Destroy(gameObject, lifetime);

    }


}