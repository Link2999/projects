﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuScript : MonoBehaviour {

	public GameObject exitmenuPanel;
	public Button playButton;
	public Button exitButton;
	public Button highscoreButton;

	private AudioSource source;
	public AudioClip startSound;
	public AudioClip exitSound;
	public AudioClip resetSound;


	void Awake()
	{
		exitmenuPanel.SetActive (false);
		source = GetComponent<AudioSource> ();
	}

	public void Play()
	{
		StartCoroutine (playSoundThenBegin ());
	}

	IEnumerator playSoundThenBegin()
	{
		source.PlayOneShot (startSound, 1);
		yield return new WaitForSeconds (startSound.length);
		SceneManager.LoadScene (1);
	}

	public void Exit()
	{
		source.PlayOneShot (exitSound, 1);
		exitmenuPanel.SetActive (true);
		playButton.interactable = false;
		exitButton.interactable = false;
		highscoreButton.interactable = false;
	}

	public void CancelExit()
	{
		playButton.interactable = true;
		exitButton.interactable = true;
		highscoreButton.interactable = true;
		exitmenuPanel.SetActive (false);
	}

	public void Quit()
	{
		Application.Quit ();
	}

	public void ResetHighScore()
	{
		source.PlayOneShot (resetSound, 1);
		PlayerPrefs.SetInt ("highscore", 0);
	}
}
