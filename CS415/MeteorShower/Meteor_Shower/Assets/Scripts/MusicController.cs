﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

	private AudioSource source;
	public AudioClip gameMusic;
	public float musicScale;

	private GameController gameController;

	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
			gameController = gameControllerObject.GetComponent<GameController> ();
		if (gameController == null)
			Debug.Log ("Cannot find 'GameController' script");
		source = GetComponent<AudioSource> ();
		//source.PlayOneShot (gameMusic, musicScale);
		source.Play();
		//source.Play(44100);
		source.loop = true;
	}
	
	void Update () {
		if (gameController.isPaused ())
			source.Pause ();
		else {
			source.UnPause ();
		}
	}
}
