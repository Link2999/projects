﻿using UnityEngine;
using System.Collections;

public class DestroyExplosion : MonoBehaviour {

	public float lifetime;

	void Start()
	{
		Destroy (gameObject, lifetime);
	}
	
}