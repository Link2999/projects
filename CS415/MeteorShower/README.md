### Version History ###


#####3/10/16#####

* Copied Previous Version to New Folder
* Removed Meteor Sprite Assets
* Removed Meteor Audio Assets
* Removed Meteor Behavior
* Removed Meteor Collisions
* Removed Basic Spawning
* Removed "Dead Zone"


#####3/4/16#####

* Sprite Assets
* Audio Assets
* Player Controls
* Projectile Behavior
* Meteor Behavior
* Projectile Collisions
* Meteor Collisions
* Basic Spawning
* "Dead Zone"

---
### To-Do ###

* Background Image/Music
* Explosion Effects
* Score Tracking & Saving
* Lives/Flags & Effects
* Single, Random Spawner
* Start Screen/Menu
* Pause Screen/Menu
* Game Over Screen/Menu
* Difficulty Tweaks
* Projectile Behavior Tweaks
* Separate "Dead Zone" Collision Detection
---
### Spikes ###

* Change Projectile Color/Sprite
* Mouse Controls
* Triggers vs Collisions
* Meteor Rotation
* Mobile Build