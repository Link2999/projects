package edu.ccsu.beans;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-12-05T21:46:12")
@StaticMetamodel(ReservationDrop.class)
public class ReservationDrop_ { 

    public static volatile SingularAttribute<ReservationDrop, Integer> rMinute;
    public static volatile SingularAttribute<ReservationDrop, String> firstName;
    public static volatile SingularAttribute<ReservationDrop, String> lastName;
    public static volatile SingularAttribute<ReservationDrop, String> ampm;
    public static volatile SingularAttribute<ReservationDrop, Integer> rMonth;
    public static volatile SingularAttribute<ReservationDrop, Integer> id;
    public static volatile SingularAttribute<ReservationDrop, String> email;
    public static volatile SingularAttribute<ReservationDrop, Integer> rDay;
    public static volatile SingularAttribute<ReservationDrop, Integer> rHour;
    public static volatile SingularAttribute<ReservationDrop, String> airport;
    public static volatile SingularAttribute<ReservationDrop, Integer> rYear;

}