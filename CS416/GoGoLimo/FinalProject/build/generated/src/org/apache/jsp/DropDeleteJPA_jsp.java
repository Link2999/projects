package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class DropDeleteJPA_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<?xml version='1.0' encoding='UTF-8' ?>\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n");
      out.write("    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\"\n");
      out.write("      xmlns:h=\"http://java.sun.com/jsf/html\"\n");
      out.write("      xmlns:f=\"http://java.sun.com/jsf/core\">\n");
      out.write("    <head>\n");
      out.write("        <title>GoGo Limo</title>\n");
      out.write("        <style type=\"text/css\">\n");
      out.write("        h2 {color: white; background-color: #1db140}\n");
      out.write("        </style>\n");
      out.write("        <link href=\"main.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("       <div id=\"outer\">\n");
      out.write("           <h1>\n");
      out.write("               <a href=\"Login.xhtml\">\n");
      out.write("                   <graphicImage url=\"/images/LimoLogo.jpg\" alt=\"GoGo Limo\"/>\n");
      out.write("               </a>\n");
      out.write("           </h1>\n");
      out.write("       </div>\n");
      out.write("       <div id=\"inner\">\n");
      out.write("        <h1>Enter ID of reservation to delete</h1>\n");
      out.write("        <form action=\"JPADropDelete\" method=\"POST\">\n");
      out.write("            Drop ID # <input type=\"text\" name=\"id\" value=\"\" /><br/>\n");
      out.write("            <input type=\"submit\" value=\"Delete\" />\n");
      out.write("        </form>\n");
      out.write("        <h2>If records are deleted the remaining records will be displayed.  \n");
      out.write("            Otherwise a page will be displayed that indicates no records were deleted\n");
      out.write("        </h2>\n");
      out.write("        <a href=\"Navigation01.xhtml\">Navigation</a>\n");
      out.write("       </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
