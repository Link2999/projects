package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class JPAUpdateReserveDrop_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("      <title>GoGo Limo</title>\n");
      out.write("      <style type=\"text/css\">\n");
      out.write("        h2 {color: white; background-color: #1db140}\n");
      out.write("      </style>\n");
      out.write("      <link href=\"main.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("    </head>\n");
      out.write("    \n");
      out.write("    <body>\n");
      out.write("        <div id=\"outer\">\n");
      out.write("          <h1>\n");
      out.write("            <a href=\"Login.xhtml\">\n");
      out.write("              <h:graphicImage url=\"/images/LimoLogo.jpg\" alt=\"GoGo Limo\"/>\n");
      out.write("            </a>\n");
      out.write("          </h1>\n");
      out.write("        </div>\n");
      out.write("      \n");
      out.write("        <div id=\"inner\">\n");
      out.write("          <h2>Update drop off Reservation</h2>\n");
      out.write("          <form action=\"JPAUpdateReserveDrop\" method=\"POST\">\n");
      out.write("            Confirm email: <input type=\"text\" name=\"email\"/><br/>\n");
      out.write("            New email:<input type=\"text\" name=\"newEmail\"/><br/>\n");
      out.write("            <input type=\"submit\" value=\"Update drop off Reservation\"/>\n");
      out.write("          </form>\n");
      out.write("          <a href=\"Navigation.xhtml\">Back to Navigation</a><br/>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
